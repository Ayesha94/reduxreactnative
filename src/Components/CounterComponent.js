import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, Alert} from 'react-native';

export default class CounterApp extends Component {

    constructor(props) {
        super(props);
    }
    
    render() {
        return(
            <View style={styles.container}>
                <Text>{this.props.count}</Text>
                <Button title="Increase Count" onPress={this.props.increment}/>
                <Button title="Decrease Count" onPress={this.props.decrement}/>
                <Button title="Counter Zero" onPress={this.props.zero}/>
            </View>
        );
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.count !== this.props.count) {
            alert("Previous State : " + this.props.count);
        }
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center', 
        top: 50
    }
});

